<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/sommaire.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'sommaire_description' => 'Générer un sommaire automatique pour les articles.',
	'sommaire_nom' => 'Sommaire automatique',
	'sommaire_slogan' => 'Un sommaire pour vos articles'
);
