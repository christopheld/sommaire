<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-sommaire?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'sommaire_description' => 'Erstellt automatisch ein Inhaltsverzeichnis eines Artikels',
	'sommaire_nom' => 'Inhaltsverzeichnis',
	'sommaire_slogan' => 'Inhaltsverzeichnisse für Ihre Artikel'
);
