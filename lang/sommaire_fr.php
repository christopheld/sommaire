<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/sommaire.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_exemple' => 'Exemple',
	'cfg_exemple_explication' => 'Explication de cet exemple',
	'cfg_titre_parametrages' => 'Paramétrages',

	// E
	'explication_niveau_max' => 'Profondeur maximale des sommaires. Cette valeur peut être changée au cas par cas via l’utilisation de la balise <code>#SOMMAIRE{#TEXTE,2}</code> dans les squelettes, ou <code>&lt;sommaire|niveau_max=2&gt;</code> dans le texte des articles.',
	'explication_numerotation_sommaire' => 'Le sommaire peut être présenté comme une liste à puces ou une liste numérotée.',
	'explication_sommaire_automatique_off' => 'Le sommaire est inséré dans le squelette par le ou la webmestre.',
	'explication_sommaire_automatique_ondemand' => 'Le sommaire est inséré uniquement en présence du raccourci <tt>&lt;sommaire&gt;</tt> dans le texte des articles.',

	// L
	'label_sommaire_automatique' => 'Insertion du sommaire d’article',
	'label_sommaire_automatique_numerote' => 'Type de liste pour le sommaire',
	'label_sommaire_automatique_numerote_off' => 'liste à puces',
	'label_sommaire_automatique_numerote_on' => 'liste numérotée',
	'label_sommaire_automatique_off' => 'Aucune insertion dans le texte des articles',
	'label_sommaire_automatique_on' => 'Insertion automatique sur tous les articles',
	'label_sommaire_automatique_ondemand' => 'Insertion dans le texte des articles à la demande',
	'label_sommaire_niveau_max' => 'Profondeur',
	'label_sommaire_niveau_max_1' => '1 niveau',
	'label_sommaire_niveau_max_2' => '2 niveaux',
	'label_sommaire_niveau_max_3' => '3 niveaux',
	'label_sommaire_niveau_max_4' => '4 niveaux',
	'label_sommaire_niveau_max_5' => '5 niveaux',
	'label_sommaire_niveau_max_6' => '6 niveaux',
	'label_sommaire_retour' => 'Où renvoient les liens de retour',
	'label_sommaire_retour_haut' => 'En haut du sommaire',
	'label_sommaire_retour_titre' => 'Sur le titre au sein du sommaire',

	// R
	'retire_sommaire' => 'Retirer le sommaire automatique',

	// S
	'selon_configuration' => 'Selon la configuration du site',
	'sommaire_titre' => 'Sommaire automatique',

	// T
	'titre_cadre_sommaire' => 'Sommaire',
	'titre_page_configurer_sommaire' => 'Sommaire automatique',
	'titre_retour_sommaire' => 'Retour au sommaire'
);
