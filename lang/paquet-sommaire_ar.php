<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-sommaire?lang_cible=ar
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'sommaire_description' => 'إنشاء فهرس آلي للمقالاتز',
	'sommaire_nom' => 'فهرس آلي',
	'sommaire_slogan' => 'فهرس لمقالاتكم'
);
