<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/sommaire?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_exemple' => 'Ejemplo',
	'cfg_exemple_explication' => 'Explicación de este ejemplo',
	'cfg_titre_parametrages' => 'Parámetros',

	// E
	'explication_niveau_max' => 'Máxima profundidad de los índice. Este valor se puede cambiar caso por caso utilizando la baliza <code>#SOMMAIRE{#TEXTE,2}</code> en los esqueletos, o <code>&lt;sommaire|niveau_max=2&gt;</code> en el texto de los artículos.
',
	'explication_numerotation_sommaire' => 'El índice se puede presentar como una lista con viñetas o una lista numerada.',
	'explication_sommaire_automatique_off' => 'El webmaster inserta un índice (sommaire) en el esqueleto.',
	'explication_sommaire_automatique_ondemand' => 'El índice se inserta solo en presencia del acceso directo <tt>&lt;sommaire&gt;</tt> en el texto del artículo.',

	// L
	'label_sommaire_automatique' => 'Insertando el sumario del artículo',
	'label_sommaire_automatique_numerote' => 'Viñetas para el índice',
	'label_sommaire_automatique_numerote_off' => 'lista de viñetas',
	'label_sommaire_automatique_numerote_on' => 'lista numerada',
	'label_sommaire_automatique_off' => 'No se inserta en el texto de los artículos.',
	'label_sommaire_automatique_on' => 'Inserción automática en todos los artículos',
	'label_sommaire_automatique_ondemand' => 'Se inserta en el texto de los artículos bajo demanda.',
	'label_sommaire_niveau_max' => 'Profundidad',
	'label_sommaire_niveau_max_1' => '1 nivel',
	'label_sommaire_niveau_max_2' => '2 niveles',
	'label_sommaire_niveau_max_3' => '3 niveles',
	'label_sommaire_niveau_max_4' => '4 niveles',
	'label_sommaire_niveau_max_5' => '5 niveles',
	'label_sommaire_niveau_max_6' => '6 niveles',
	'label_sommaire_retour' => '¿A dónde van los enlaces?',
	'label_sommaire_retour_haut' => 'Parte superior del índice',
	'label_sommaire_retour_titre' => 'En el título dentro del índice',

	// S
	'sommaire_titre' => 'Índice automático',

	// T
	'titre_cadre_sommaire' => 'Índice',
	'titre_page_configurer_sommaire' => 'Configurar Índice automático',
	'titre_retour_sommaire' => 'Volver al índice'
);
